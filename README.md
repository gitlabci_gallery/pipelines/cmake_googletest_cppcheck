# Gitlab-ci pipelines

A gitlab-ci pipeline is a continuous integration (CI) workflow composed of a list of stages that are run
sequentially and in the order defined in your [.gitlab-ci.yml](.gitlab-ci.yml) file. If a stage fails, the next
stages won't be executed.
Each stage is composed of zero to `N` jobs that are run in parallel.

There is no *better* way to design your pipelines, but the message to take away is that the faster your CI
result is available, the faster you will be aware of issues, and the faster the issues will be fixed.

# Docker/CMake/GoogleTest/CppCheck pipeline example

Let's take an easy to understand C++ example and see how to configure a pipeline to build and test this code.

## Code source

The code is a random numbers generator taken from
[this github repository](https://github.com/hANSIc99/cpp_testing_sample).
Copyright is [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) -
[Stephan Avenwedde](https://opensource.com/users/hansic99)

An executable `Producer`:
* takes a number `N` as argument
* calls `Generator::generate` that generates a range of N unique shuffled numbers from 1 to `N`
* outputs the range

```bash
$ ./Producer 3
3 1 2
```

Unitary tests in `Generator/GeneratorTest.cpp` is using GoogleTest to test the function `Generator::generate` at its
limits.

If you want some explanation about the C++ code itself, the CMake part and how to build/debug the project with
VSCodium, please refer to [this related article](https://opensource.com/article/22/1/devops-cmake).

If case you need to know more about [GoogleTest](https://google.github.io/googletest/) (a C++ framework for unitary
tests and mocking), [Ctest](https://cmake.org/cmake/help/latest/manual/ctest.1.html) (a CMake utility to launch unitary
tests) and how to launch these tools, all is explained in
[this second article](https://opensource.com/article/22/1/unit-testing-googletest-ctest).

## Pipeline designing

### First stage - Docker

In our CI pipeline, the CI first needs to build a docker image with all the build and test tools required by the
project (make, CMake, g++, cppcheck and git - googletest is installed thanks to cmake
[FetchContent feature](https://cmake.org/cmake/help/latest/module/FetchContent.html)).
This will be the first stage of our pipeline (called `docker` in [.gitlab-ci.yml](.gitlab-ci.yml)), and this stage will
be launched only if the [Dockerfile](Dockerfile) is modified. This is an independant stage, because if this stage is
failing, there is no point to try to build the sources.

### Second stage

Then the project and unitary tests needs to be built. This can be done thanks to CMake/make following commands:

```bash
mkdir build # building elsewhere than in the source directory is a good practice
cd build
cmake ..
make
```

The unitary tests need to be launched:
```bash
# in former step build directory
ctest
```

One can also launch `./Generator/GeneratorTest`, but [CTest](https://cmake.org/cmake/help/latest/manual/ctest.1.html)
is integrated to CMake and has nice launch options/features.

Finally, the CI will launch [CppCheck](http://cppcheck.net/), a static C++ code analyser, that is a good tool to
improve C and C++ code quality.

If we want something fast, one of the best option is to define the following jobs (parallel execution) in another
stage:
* build and test the executable
* build and launch unitary tests
* run [CppCheck](http://cppcheck.net/)

[This final article](https://opensource.com/article/22/2/setup-ci-pipeline-gitlab) explains how to configure pipelines
on https://gitlab.com. This part was slightly adapted for the use of docker and to make the CI faster by avoiding to
save `Producer` artifact and reloading it in another stage. 

### The final pipeline

```mermaid
graph LR;
  subgraph docker
  B(build_docker_image);
  end
  subgraph test
  C(build_and_test_producer);
  D(unitary_tests);
  E(static_analysis);
  end
  B-->C
  B-->D
  B-->E
```
